﻿using System;
using System.Collections.Generic;
using System.Text;
using Viewboard.Library.Authentication.Model;

namespace Viewboard.Library.Authentication.Services
{
    public interface IAuthService
    {
        AuthData GetAuthData(string id);
        string HashPassword(string password);
        bool VerifyPassword(string actualPassword, string hashedPassword);
    }
}
