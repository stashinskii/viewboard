﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Viewboard.Library.Predictions.Models.Logo
{
    public class ImagePrediction
    {
        //[ColumnName(TFModelScorer.InceptionSettings.outputTensorName)]
        public float[] PredictedLabels;
    }
}
