﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viewboard.Library.Predictions.ModelLoading.Logo
{
    public interface IModelLoader
    {
        void LoadModel();
    }
}
