﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Viewboard.Library.Entities.Models;

namespace Viewboard.Library.Data.Repository
{
    public interface IUserRepository
    {
        void Update(User entity);
        User GetSingle(string id);
        User GetSingle(Expression<Func<User, bool>> predicate);
        IEnumerable<User> GetAll();
        void Delete(User entity);
        void Add(User entity);
        bool isEmailUniq(string email);
        bool IsUsernameUniq(string username);
        void Commit();
    }
}
