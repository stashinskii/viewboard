﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Viewboard.Library.Data.Context;
using Viewboard.Library.Entities.Models;

namespace Viewboard.Library.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        UserContext context;

        public UserRepository(UserContext context)
        {
            this.context = context;
        }

        public void Add(User entity)
        {
            EntityEntry dbEntityEntry = context.Entry<User>(entity);
            context.Set<User>().Add(entity);
        }

        public void Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            throw new Exception();
        }

        public User GetSingle(string id)
        {
            return context.Set<User>().FirstOrDefault(x => x.Id == id);
        }

        public User GetSingle(Expression<Func<User, bool>> predicate)
        {
            return context.Set<User>().FirstOrDefault(predicate);
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }

        public bool isEmailUniq(string email)
        {
            var user = this.GetSingle(u => u.Email == email);
            return user == null;
        }

        public bool IsUsernameUniq(string username)
        {
            var user = this.GetSingle(u => u.Username == username);
            return user == null;
        }

        public void Commit()
        {
            context.SaveChanges();
        }

    }
}
